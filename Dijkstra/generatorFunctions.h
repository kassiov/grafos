//##############################################################################################################################################//
//								$ GERADORES DE GRAFOS $																							//
//##############################################################################################################################################//
/*
#################################################################################################################################################
#
#	>>> Trabalho Final de Grafos;																													#
#
#	>>> Participantes:																																#
#			> EDSON MACHADO MOITA NETO;																												#
#			> PABLO VINICIUS DA CRUZ LIMA;																											#
#			> KASSIO VENICIOS ALVES CARVALHO;																										#
#			> MARCOS JOSHOA OLIVEIRA PARENTES.																										#
#
#################################################################################################################################################
*/

//##############################################################################################################################################//
//							+ GERADOR DE GRAFO ORIENTADO +																						//
//##############################################################################################################################################//

Grafo gerarGrafoOrientado(int qtdV, int qtdA);

int geradorGrOrientado() {

	int qtdV, qtdG, qtdA, i, j, k;
  char nomeArquivo[100];

  srand((unsigned)time(NULL));

  printf("Deseja gerar quantos grafos?");
  scanf("%d", &qtdG);
  printf("Com quantos nos?");
  scanf("%d", &qtdV);
  printf("Com quantos arcos?");
  scanf("%d", &qtdA);

  Grafo G;

  int qtdMax, qtdMin;

  qtdMin = qtdV - 1;						// M�nimmo de arestas para um grafo conectado.
  qtdMax = (qtdV * (qtdV-1))/2; // m�ximo de arestas (grafo completo).
  qtdMax = qtdMax - (qtdV-1);		// n�o pode haver arcos partindo do n� destino. FATO.

  // Caso o usu�rio escolha uma quantidade de arcos invi�vel � feita uma corre��o.
  if(qtdA < qtdMin || qtdA > qtdMax) {
  	printf("qtdMax = %d\nqtdMin = %d\n", qtdMax, qtdMin);
		qtdA = (qtdMax - qtdMin)/2;
	}


  for(i = 0; i < qtdG; i++) {
		G = gerarGrafoOrientado(qtdV, qtdA);

		// Mostra a quantidade N�s e Arcos do Grafo gerado
		printf("\nGrafo salvo\n");
	  qtdA = 0;
	  for(j = 0; j < G.qtdV; j++) {
			for(k = 0; k < G.qtdV; k++) {
				if(G.matAdj[j][k] != -1) {
					qtdA++;
				}
			}
		}
		printf("Nos = %d\nArcos = %d\n", G.qtdV, qtdA);

		// Montar o nome do arquivo de acordo com as escolhas do us�rio. O - Orientado.
	  montarNomeArquivo(i+1, qtdV, 'O', 0, nomeArquivo);

	  // Gravar o Grafo em um arquivo TXT com o nome montado.
	  gravarGrafo(G, nomeArquivo);

	  desalocarMatrizAdj(G.qtdV, G.matAdj);

	  printf("\nCriado o arquivo: ");
	  puts(nomeArquivo);
	}

	return 0;
}

Grafo gerarGrafoOrientado(int qtdV, int qtdA) {

	Grafo G;
	int i, custo, custoMin, custoMax, n1, n2, escolhidos[qtdV], qtdEsc = 0;

	custoMin = 0;
	custoMax = 10*qtdA;

	G.matAdj = alocarMatrizAdj(qtdV);
	G.qtdV = qtdV;

	// Garantir a conex�o do Grafo. � criado um percurso iniciando no n� fonte(0).
	n1 = 0;
	escolhidos[qtdEsc] = n1;
	qtdEsc++;
  for(i = 0; i < G.qtdV-1; i++) {

  	/* Gerar um n� n2, aonde chega o arco que sai de n1, que ainda n�o tenha
  	sido escolhido evitando la�os, ciclos e que n�o seja uma liga��o direta
  	fonte -> destino.
		*/
    do {
    	if(i == 0) {
				n2 = gerarInteiroAleatorio(0, G.qtdV-1-1); // excluo o n� destino.
			} else {
				n2 = gerarInteiroAleatorio(0, G.qtdV-1);
			}
		} while(foiEscolhido(n2,qtdEsc,escolhidos));
		escolhidos[qtdEsc] = n2;
		qtdEsc++;

		// Inserir custo aleat�rio no arco formado pelo n� n1 e n2.
    custo = gerarInteiroAleatorio(custoMin, custoMax);
    G.matAdj[n1][n2] = custo;

    // N� n1 torna-se n2 para manter grafo conectado.
    n1 = n2;
  }

	// Inserir arestas remanescentes ap�s garantir a conex�o.
	qtdA = qtdA - (G.qtdV-1);
	for(i = 0; i < qtdA; i++) {
		do {

			// O n� de onde parte o arco nunca pode ser o n� destino. FATO.
    	n1 = gerarInteiroAleatorio(0, G.qtdV-1-1);

    	// O n� aonde chega o arco n�o pode ser o fonte. ASSUMIMOS.
			n2 = gerarInteiroAleatorio(1, G.qtdV-1);
		} while(n1 == n2 || G.matAdj[n1][n2] != -1 || (n1 == 0 && n2 == G.qtdV-1));

		// Inserir custo aleat�rio no arco formado pelo n� n1 e n2.
		custo = gerarInteiroAleatorio(custoMin, custoMax);
	  G.matAdj[n1][n2] = custo;
	}

	return G;
}
