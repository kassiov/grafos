//##############################################################################################################################################//
//									$ FUNCOES DE AUXILIO $																								                                                      //
//##############################################################################################################################################//
/*
#################################################################################################################################################
#                                                                                                                                               #
#	>>> Trabalho Final de Grafos;																													                                                        #
#                                                                                                                                               #
#	>>> Participantes:																																                                                            #
#			> EDSON MACHADO MOITA NETO;																												                                                        #
#			> PABLO VINICIUS DA CRUZ LIMA;																											                                                      #
#			> KASSIO VENICIOS ALVES CARVALHO;																										                                                      #
#			> MARCOS JOSHOA OLIVEIRA PARENTES.																										                                                    #
#                                                                                                                                               #
#################################################################################################################################################
*/

//##############################################################################################################################################//
//									+ DEFINES +																								                                                                  //
//##############################################################################################################################################//

#define INFINITO 0x3f3f3f

//##############################################################################################################################################//
//									+ TYPEDEFS +																							                                                                 	//
//##############################################################################################################################################//

typedef struct
{
	int id;					// N�mero do v�rtice (1...qtdV)
	int visitado;		// Status de visitado (0-n�o visitado ou 1-visitado)
} Vertice;

typedef struct
{
	Vertice v1;			// V�rtice origem - representa a linha na matAdj
	Vertice v2;			// V�rtice destino - representa a coluna na matAdj
	int peso;				// Peso da aresta
} Aresta;

typedef struct
{
	int qtdV;				// Quantidade de v�rtices do Grafo
	int **matAdj;		// Matriz de Adjac�ncias (-1 se n�o existir aresta em [i][j]
} Grafo;						//											peso se     existir aresta em [i][j])

//##############################################################################################################################################//
//									+ FUNCOES +																									                                                                //
//##############################################################################################################################################//

int lerQtdV(char nomeArquivo[100]);
void lerMatAdjTXT(char nomeArquivo[100], int qtd, int mat[qtd][qtd]);
void verificarLeitura(Grafo G);
void imprimirMatriz(int tam, int **mat);
Grafo extrairGrafoDeMatriz(int tamanho, int mat[tamanho][tamanho]);
int contarArestas(int tam, int **mat);
int** alocarMatrizAdj(int ordem);
void desalocarMatrizAdj(int ordem, int **m);
int calcularPeso(Grafo T);
int gerarInteiroAleatorio(int limInferior, int limSuperior);
int verticePertence(int tamanho, Vertice W[tamanho], int vertice);
void inserirAresta(Aresta nova, Grafo *T, Vertice W[T->qtdV]);
int foiEscolhido(int id, int t, int vet[t]);
void montarNomeArquivo(int idGrafo, int qtdV, char tipoGrafo, int densidade, char nomeArquivo[100]);
void gravarGrafo(Grafo G, char nomeArquivo[100]);

//##############################################################################################################################################//
//									+ IMPLEMENTACAO +																							                                                              //
//##############################################################################################################################################//

//##############################################################################################################################################//
int lerQtdV(char nomeArquivo[100]) {
	
	/* Ler o arquivo para determinar a quantidade de v�rtices e criar as estruturas
	de vetor e/ou matriz com tamanho determinado que s�o necess�rias para execu��o.
	*/
	int qtd;
	FILE* arq;

  if((arq = fopen(nomeArquivo, "r")) == NULL){
    printf("Erro ao abrir o arquivo.\n");
  } else {
    fscanf(arq, "%d", &qtd);
    fclose(arq);
  }
  
  return qtd;
}

//##############################################################################################################################################//
void lerMatAdjTXT(char nomeArquivo[100], int qtd, int mat[qtd][qtd]) {

	FILE* arq;
  int i, j, v, peso;

  if((arq = fopen(nomeArquivo, "r")) == NULL){
    printf("Erro ao abrir o arquivo.\n");
  } else {
    fscanf(arq, "%d", &v);
    for(i = 0; i < qtd; i++) {
			for(j = 0; j < qtd; j++) {
				fscanf(arq, "%d", &peso);
				mat[i][j] = peso;
			}
		}
    fclose(arq);
  }	
}

//##############################################################################################################################################//
void verificarLeitura(Grafo G) {

  int qtdE;
  
  qtdE = contarArestas(G.qtdV, G.matAdj);

  printf("\n|V| = %d\n|E| = %d\n", G.qtdV, qtdE);
  
}

//##############################################################################################################################################//
void imprimirMatriz(int tam, int **mat) {

  int i, j;

  printf("\nMatriz\n");
  for(i = 0; i < tam; i++) {
    for(j = 0; j < tam; j++) {
      printf("%2d ", mat[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

//##############################################################################################################################################//
Grafo extrairGrafoDeMatriz(int tamanho, int mat[tamanho][tamanho]) {

	/* A partir de uma matriz de adjac�ncias monta um Grafo. Funciona principalmente
	para copiar valores entre as matrizes.
	*/
  int i, j;
  Grafo G;

  G.qtdV = tamanho;
  
  G.matAdj = alocarMatrizAdj(tamanho);

  for(i = 0; i < tamanho; i++) {
    for(j = i; j < tamanho; j++) {
      G.matAdj[i][j] = mat[i][j];
      G.matAdj[j][i] = mat[j][i];
    }
  }

  return G;
}

//##############################################################################################################################################//
int contarArestas(int tam, int **mat) {
	
	int i, j, qtdE = 0;
	
	for(i = 0; i < tam; i++) {
		for(j = i; j < tam; j++) {
			if(mat[i][j] != -1) {
				qtdE++;
			}
		}
	}
	
	return qtdE;
}

//##############################################################################################################################################//
int** alocarMatrizAdj(int ordem) {
	
	int i,j;
	
	int **m = (int**)malloc(ordem * sizeof(int*));
	
	for(i = 0; i < ordem; i++) {
		m[i] = (int*)malloc(ordem * sizeof(int));
		for(j = 0; j < ordem; j++) {
			m[i][j] = -1;
		}
	}
	
	return m;
}

//##############################################################################################################################################//
void desalocarMatrizAdj(int ordem, int **m) {
	
	int i;
	
	for(i = 0; i < ordem; i++) {
		free(m[i]);
	}
	free(m);
}

//##############################################################################################################################################//
int calcularPeso(Grafo T) {

	// Calcula o Peso da �rvore(T-Tree) para os algoritmos de Prim e Kruskal.
  int i, j, peso = 0;

  for(i = 0; i < T.qtdV; i++) {
    for(j = i+1; j < T.qtdV; j++) {
      if(T.matAdj[i][j] != -1) {
        peso += T.matAdj[i][j];
      }
    }
  }

  return peso;
}

//##############################################################################################################################################//
int gerarInteiroAleatorio(int limInferior, int limSuperior) {

	// Gerar um inteiro aleat�rio entre os limites(inclusive) passados.
  int num, amplitude;
  
  amplitude = limSuperior-limInferior;

  num = limInferior + (rand() % (amplitude+1));

  return num;
}

//##############################################################################################################################################//
int verticePertence(int tamanho, Vertice W[tamanho], int vertice) {

  int i;

  for (i = 0; i < tamanho; i++) {
    if(W[i].id == vertice) {
      return 1;
    }
  }

  return 0;
}

//##############################################################################################################################################//
void inserirAresta(Aresta nova, Grafo *T, Vertice W[T->qtdV]) {

  /* Inserir v�rtices. Caso o v�rtice n�o perten�a ao conjunto passado, ele ser�
	inserido e a quantidade de v�rtices incrementada em 1.
	*/
  if(!verticePertence(T->qtdV, W, nova.v1.id)) {
    W[T->qtdV] = nova.v1;
    T->qtdV = T->qtdV + 1;
  }
  if(!verticePertence(T->qtdV, W, nova.v2.id)) {
    W[T->qtdV] = nova.v2;
    T->qtdV = T->qtdV + 1;
  }
  
  //Atualizar a Matriz de Adjac�ncias com o valor do peso da nova aresta.
  T->matAdj[nova.v1.id-1][nova.v2.id-1] = nova.peso;
  T->matAdj[nova.v2.id-1][nova.v1.id-1] = nova.peso;

}

//##############################################################################################################################################//
int foiEscolhido(int id, int t, int vet[t]) {
	
	/* Verificar se o v�rtice j� foi escolhido para comp�r o percurso que	vai 
	garantirConexao() do Grafo a ser gerado.*/
	
	int i;
	for(i = 0; i < t; i++) {
		if(vet[i] == id) {
			return 1;
		}
	}
	return 0;
}

//##############################################################################################################################################//
void montarNomeArquivo(int idGrafo, int qtdV, char tipoGrafo, int densidade, char nomeArquivo[100]) {

	/* Montar nome do arquivo de acordo com as escolhas do usu�rio.
	G - Grafo
	C - Grafo Completo
	Q - Grafo Qualquer
	O - Grafo Orientado
	D - Densidade do Grafo(1...4)
	xV - Quantidade x de v�rtices
	#y - N�mero do Grafo. Para distinguir quando forem gerados v�rios.
	*/
  char str[100];
  
  sprintf(nomeArquivo, "G-%c-", tipoGrafo);
  if(tipoGrafo == 'Q') {
    sprintf(str, "D%d-", densidade);
  	strcat(nomeArquivo, str);
  }
  sprintf(str, "%dV-#%d.TXT", qtdV, idGrafo);
  strcat(nomeArquivo, str);
  
}

//##############################################################################################################################################//
void gravarGrafo(Grafo G, char nomeArquivo[100]) {
	
	// Gravar Grafo em arquivo TXT de acordo com o nome montado.
  FILE* arq;
  int i, j;
  
  arq = fopen(nomeArquivo, "wb");
  
  fprintf(arq, "%d", G.qtdV);
  for(i = 0; i < G.qtdV; i++) {
  	fprintf(arq,"\n");
		for(j = 0; j < G.qtdV; j++) {
			fprintf(arq, "%d ", G.matAdj[i][j]);
		}
	}
  fclose(arq);
}