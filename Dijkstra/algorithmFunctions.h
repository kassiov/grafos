//##############################################################################################################################################//
//									$ ALGORITMOS $																								//
//##############################################################################################################################################//
/*
#################################################################################################################################################
#
#	>>> Trabalho Final de Grafos;																													#
#
#	>>> Participantes:																																#
#			> EDSON MACHADO MOITA NETO;																												#
#			> PABLO VINICIUS DA CRUZ LIMA;																											#
#			> KASSIO VENICIOS ALVES CARVALHO;																										#
#			> MARCOS JOSHOA OLIVEIRA PARENTES.																										#
#
#################################################################################################################################################
*/

//##############################################################################################################################################//
//								+ ALGORITMO DE DIJKSTRA +																						//
//##############################################################################################################################################//

typedef struct No{
  int id;					// n�mero que identifica o v�rtice (1 a qtdV).
  int rotulo;			// valor do v�rtice/distância para a origem (0 a Infinito).
  int fechado;		// status na execu��o do algoritmo(1 - fechado/0 - aberto).
  int predecessor;// id do v�rtice anterior a este no caminho candidato � m�nimo.
}No;

void algoritmoDijkstra(int qtdV, int matAdj[qtdV][qtdV], No R[qtdV]);
No noDeMenorValor(int qtdV, No NR[qtdV]);
No getNo(int id, int qtdV, No R[qtdV]);
void ordenarNos(int qtd, No R[qtd]);

int algDijkstra() {

	int i, outroGrafo, qtdV;
  char nomeArquivo[100];
  double tempoGasto;
  clock_t tInicial, tFinal;

  srand((unsigned)time(NULL));

  do{
		printf("Qual o nome do arquivo?\n");
	  scanf("%s", nomeArquivo);

	  // Ler a quantidade de v�rtices do Grafo no arquivo TXT.
		qtdV = lerQtdV(nomeArquivo);
		int matAdj[qtdV][qtdV];

		// Ler a Matriz de Adjac�ncias do arquivo.
		lerMatAdjTXT(nomeArquivo, qtdV, matAdj);
		No R[qtdV];	// Conjunto de N�s Rotulados.

		// Executar algoritmo cronometrando o tempo.
		printf("\nExecutando algoritmo...\n");
	  tInicial = clock();
	  algoritmoDijkstra(qtdV, matAdj, R);
	  tFinal = clock();

		/* Fim do algoritmo. O conjunto R cont�m todos os n�s com os r�tulos e
		predecessores atualizados. O Caminho M�nimo � o r�tulo do N� Destino.
		*/
	  No destino;
	  destino = getNo(qtdV,qtdV,R);
	  printf("\n\nCaminho Minimo = %d\n", destino.rotulo);

	  /* Agora deve-se encontrar o n�s que comp�e o Caminho M�nimo. Isso � feito
	  a partir do n� destino fazendo uma volta atrav�s dos predecessores at� chegar
	  no n� fonte que � aquele que n�o tem predecessor (== -1).
	  */
	  int nosCaminho = 0;
	  No caminho[qtdV-1];
		caminho[nosCaminho] = destino;
		nosCaminho++;
	  while(destino.predecessor != -1) {
	    destino = getNo(destino.predecessor,qtdV,R);
	    caminho[nosCaminho] = destino;
	    nosCaminho++;
	  }

	  // N�s devem ser ordenados para dar o caminho no sentido fonte->destino.
	  ordenarNos(nosCaminho,caminho);

	  // Impress�o do Caminho M�nimo.
	  for(i = 0; i < nosCaminho; i++) {
	    printf("%d", caminho[i].id);
	    if(i == nosCaminho-1) {
				printf("\n");
			} else {
				printf(" - ");
			}
	  }

		// Mostrar tempo gasto pelo algoritmo.
	  tempoGasto = ( (double) (tFinal - tInicial) ) / CLOCKS_PER_SEC;
	  printf("\nDemorou %lf segundos.\n", tempoGasto);

	  printf("\nDeseja solucionar outro grafo? 0 - Nao/1 - Sim\n");
	  scanf("%d", &outroGrafo);
	  printf("\n");
	} while(outroGrafo);

  return 0;
}

void algoritmoDijkstra(int qtdV, int matAdj[qtdV][qtdV], No R[qtdV]) {

	No k;
  No j;
  int custo;
  int valor;
  int i;

  /* IN�CIO
  R = {} - conjunto dos n�s rotulados(fechados)
  NR = {1...n} - conjunto dos n�s N�O rotulados
  */
  No NR[qtdV];
  int qtdRot, qtdNaoRot;
  qtdRot = 0;
  qtdNaoRot = qtdV;
  // "Inserir" os v�rtices no conjunto n�o rotulado
  for(i = 0; i < qtdNaoRot; i++) {
    NR[i].id = i+1; // vai de 1 a n(qtdV), "inserindo" em ordem
    NR[i].fechado = 0; // devem estar como abertos
  }

  /*PASSO 1
  Atribua o valor 0 ao n� fonte e INFINITO aos demais n�s
  */
  NR[0].rotulo = 0;
  NR[0].predecessor = -1; // o n� fonte n�o tem n� predecessor pois � o primeiro
  for(i = 1; i < qtdNaoRot; i++) {
    NR[i].rotulo = INFINITO;
  }

  /*PASSO 2
  Enquanto o conjunto de n�s n�o rotulados for n�o vazio (NR!={}) fa�a o seguinte:
    - Selecione o n� ainda n�o rotulado com menor valor(n� k)
    - Passe o n� k para o conjunto de n�s rotulados
    - Para todo n� j ainda n�o rotulado que seja sucessor de k, fa�a:
      - Some o valor do n� k com o custo do arco que une os n�s k e j, e atribua
			  esse novo valor ao n� j em caso de melhoria. Nesse caso, define-se o n�
				k como precedente de j(s� se houver melhoria).
  */
  while(qtdNaoRot != 0) {

    k = noDeMenorValor(qtdV, NR); // O N� � fechado nesta chamada de fun��o.
    R[qtdRot] = k;
    qtdRot++;
    qtdNaoRot--;

    for(i = 0; i < qtdV; i++) {

      if(!NR[i].fechado){	// N�o fechado == aberto == n�o rotulado
        j = NR[i];
        custo = matAdj[k.id-1][j.id-1];

        /* Garantir que j � sucessor de k. S� � sucessor se existe um arco que
        sai de k e chegue em j. Logo, na Matriz de Adjacencias[k][j] o valor
        deve ser !=-1, >=0.
				*/
        if(custo >= 0) {
          valor = k.rotulo + custo;

          // S� h� melhoria quando o valor calculado � menor do que o r�tulo atual.
          if(valor < j.rotulo) {
					  j.rotulo = valor;
            j.predecessor = k.id;
            NR[i] = j; 		// como manipulo NR, ao final "atualizo" o N�.
          }
        }
      }
    }
  }

  /* O algoritmo acaba aqui. Ao final temos o conjunto R com todos os n�s e seus
	r�tulos (valores-distancias ate a origem).
	*/
}

No noDeMenorValor(int qtdV, No NR[qtdV]) {

  int i;
  int menorValor = INFINITO;
  int pos;

  // Procurar a posi��o do N� de menor valor(r�tulo) que ainda n�o foi fechado(rotulado).
  for(i = 0; i < qtdV; i++) {
      if(NR[i].rotulo < menorValor && !NR[i].fechado) {
        menorValor = NR[i].rotulo;
        pos = i;
      }
  }

  // Fecha o n� dessa posi��o pois ele ser� utilizado no algoritmo, inserido em R.
  NR[pos].fechado = 1;

  // Retorna o n� de menor valor j� fechado.
  return NR[pos];
}

No getNo(int id, int qtdV, No R[qtdV]) {

  int i;

  // Dado um id de n�, devolve o n� correspondente.
  for(i = 0; i < qtdV; i++) {
    if(R[i].id == id) {
      return R[i];
    }
  }

}

void ordenarNos(int qtd, No R[qtd]) {

	int i, j;
	No aux;

	// Ordenar n�s no sentido fonte(0)->destino(qtd). Ordem crescente de id.
  for(i = 0; i< qtd; i++) {
    for(j = 0; j < qtd-1; j++) {
      if(R[j].id > R[j + 1].id) {
        aux = R[j];
        R[j] = R[j+1];
        R[j+1] = aux;
      }
    }
  }
}
