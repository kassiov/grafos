//##############################################################################################################################################//
//							$ FUNCOES PARA EXECU�AO DE PROGRAMA $																																														//
//##############################################################################################################################################//
/*
#################################################################################################################################################
#																																																																								#
#	>>> Trabalho Final de Grafos;																																																									#
#																																																																								#
#	>>> Participantes:																																																														#
#			> EDSON MACHADO MOITA NETO;																																																								#
#			> PABLO VINICIUS DA CRUZ LIMA;																																																						#
#			> KASSIO VENICIOS ALVES CARVALHO;																																																					#
#			> MARCOS JOSHOA OLIVEIRA PARENTES.																																																				#
#																																																																								#
#################################################################################################################################################
*/

//##############################################################################################################################################//
//								+ MOSTRAR NA TELA +																																																						//
//##############################################################################################################################################//
void top1()
{
	printf("\t\t\t\t      ++++++++++++++++++++++++\n\t\t\t\t      ");
	printf("+ ALGORITMOS DE GRAFOS +");
	printf("\n\t\t\t\t      ++++++++++++++++++++++++\n");
}

void top2()
{
	printf("  \t\t\t\t            +++++++++++++++++++++\n\t\t\t\t            ");
	printf("+ GERADOR DE GRAFOS +");
	printf("\n\t\t\t\t            +++++++++++++++++++++\n");
}

void top3()
{
	printf("  \t\t\t\t          ++++++++++++++++++++++++\n\t\t\t\t          ");
	printf("+ EXECUTAR ALGORITMOS +");
	printf("\n\t\t\t\t          ++++++++++++++++++++++++\n");
}

void top4(){
	printf("  \t\t\t\t          ++++++++++++++++++++++++\n\t\t\t\t          ");
	printf("+ FINALIZANDO PROGRAMA +");
	printf("\n\t\t\t\t          ++++++++++++++++++++++++\n");
}

void menu1(int *op)
{
	top1();
	printf("\n\t\t     -------------------- \t\t  ---------------------------");
	printf("\n\t\t     > 1-> Gerar Grafos < \t\t  > 2-> Executar Algoritmos <\n\t\t     ");
	printf("-------------------- \t\t  ---------------------------\n\t\t\t\t\t   -------------");
	printf("\n\t\t\t\t\t   > 0-> Sair <\n");
	printf("\t\t\t\t\t   -------------\n\t\t     ********\n\t\t     >>$ ");
	scanf("%d", &(*op));
	clearScr();
}

void menu3(int *op)
{
	top3();
	printf("\n\t\t\t    ------------ \t---------------");
	printf("\n\t\t\t    > 1-> PRIM < \t> 2-> KRUSKAL <\n");
	printf("\t\t\t    ------------ \t---------------");
	printf("\n\n\t\t\t\t\t         -------------");
	printf("\n\t\t\t\t\t         > 0-> Sair <\n");
	printf("\t\t\t\t\t         -------------");
	printf("\n\t\t\t      ********");
	printf("\n\t\t\t      >>$ ");
	scanf("%d", &(*op));
	clearScr();
}

void clearScr() {
    //"limpar" a tela;
	system("clear || cls");
}

void msg() {
    //mensagem qualquer;
	printf("\n\t\t\t\tPRESSIONE QUALQUER TECLA PARA CONTINUAR!\n");
}

void mjopers() {
	printf("\n\n\t\t# Programadores:   ");
	printf(">>> EDSON MACHADO MOITA NETO;");
	printf("\n\t\t----------------");
	printf("\n\t\t\t\t   >>> PABLO VINICIUS DA CRUZ LIMA;");
	printf("\n\n\t\t\t\t   >>> KASSIO VENICIOS ALVES CARVALHO;");
	printf("\n\n\t\t\t\t   >>> MARCOS JOSHOA OLIVEIRA PARENTES.\n\n");
    msg();
    getch();
	clearScr();
}

//##############################################################################################################################################//
//								+ EXECUCAO +																																																									//
//##############################################################################################################################################//

void geradExec()
{
    top2();
    geradorGrNOrintado();
    msg();
    getch();
    clearScr();
}

void algExec()
{
	int op = 0;
	do
	{
		menu3(&op);
		switch(op) {
		case 1:
			algPrim();
			msg();
			getch();
			clearScr();
			break;
		case 2:
			algKruskal();
			msg();
			getch();
			clearScr();
			break;
		default:
			break;
		}
	}
	while(op != 0);
}

void iniciar()
{
	int op = 0;
	do
	{
		menu1(&op);
		switch(op) {
        case 1: geradExec(); break;
        case 2: algExec(); break;
        default: break;
		}
	}
	while(op != 0);

}
 void fechar(){
	 top4();
	 mjopers();
 }
