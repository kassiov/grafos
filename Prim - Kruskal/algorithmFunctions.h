//##############################################################################################################################################//
//									$ ALGORITMOS $																																																							//
//##############################################################################################################################################//
/*
#################################################################################################################################################
#																																																																								#
#	>>> Trabalho Final de Grafos;																																																									#
#																																																																								#
#	>>> Participantes:																																																														#
#			> EDSON MACHADO MOITA NETO;																																																								#
#			> PABLO VINICIUS DA CRUZ LIMA;																																																						#
#			> KASSIO VENICIOS ALVES CARVALHO;																																																					#
#			> MARCOS JOSHOA OLIVEIRA PARENTES.																																																				#
#																																																																								#
#################################################################################################################################################
*/

//##############################################################################################################################################//
//								+ ALGORITMO DE PRIM +																																																					//
//##############################################################################################################################################//

Grafo algoritmoPrim(Grafo G);
int conjuntoDesconectanteVazio(Grafo G, int tam, Vertice W[tam]);
Aresta encontrarArestaPesoMin(Grafo G, Grafo T, Vertice W[T.qtdV]);

int algPrim() {

	int qtd, peso, outroGrafo;
	double tempoGasto;
	char nomeArquivo[100];
	clock_t tInicial, tFinal;
	Grafo G, T;

	srand((unsigned)time(NULL));

	do {
		printf("Qual o nome do arquivo?\n");
		scanf("%s", nomeArquivo);

		// Ler a quantidade de v�rtices do Grafo no arquivo TXT.
		qtd = lerQtdV(nomeArquivo);
		int matAdj[qtd][qtd];

		// Ler a Matriz de Adjac�ncias do arquivo.
		lerMatAdjTXT(nomeArquivo, qtd, matAdj);

		// Copiar a Matriz de Adjac�ncias para o Grafo.
		G = extrairGrafoDeMatriz(qtd, matAdj);

		// Mostrar tamanho do Grafo.(Quantidade de V�rtices e Arestas)
		printf("\nGrafo G\n");
		verificarLeitura(G);

		// Executar algoritmo cronometrando o tempo.
		printf("\nExecutando algoritmo...\n");
		tInicial = clock();
		T = algoritmoPrim(G);
		tFinal = clock();

		desalocarMatrizAdj(G.qtdV, G.matAdj);

		printf("\nArvore T\n");
		verificarLeitura(T);
		imprimirArvore(T);

		// Calcular peso da �rvore Geradora M�nima T(Tree)
		peso = calcularPeso(T);
		printf("Peso = %d\n", peso);

		desalocarMatrizAdj(T.qtdV, T.matAdj);

		// Mostrar tempo gasto pelo algoritmo.
		tempoGasto = ( (double) (tFinal - tInicial) ) / CLOCKS_PER_SEC;
		printf("\nDemorou %lf segundos.\n", tempoGasto);

		printf("\nDeseja solucionar outro grafo? 0 - Nao/1 - Sim\n");
		scanf("%d", &outroGrafo);
		printf("\n");
	} while(outroGrafo);

	return 0;
}

Grafo algoritmoPrim(Grafo G) {

	Aresta aMin;
	Vertice W[G.qtdV];
	int qtdE = 0;

	/*
	PASSO 1
	Selecione um v�rtice arbitr�rio do Grafo G, e insira ele em T = {}
	*/
	Grafo T;
	T.matAdj = alocarMatrizAdj(G.qtdV);

	W[0].id = gerarInteiroAleatorio(1, G.qtdV);
	T.qtdV = 1;

	/*
	PASSO 2
	Seja W o conjunto de v�rtices em T. Se o conjunto desconectante (W, V-W) � vazio,
	G n�o � conectado, PARE. Caso contr�rio, encontre uma aresta de peso m�nimo no
	conjunto desconectante (W, V-W). Insira ela em T e v� para o PASSO 3.
	PASSO 3
	Se T possui (N-1) arestas, PARE. Caso contr�rio v� para o PASSO 2.
	*/
	while(qtdE != G.qtdV - 1) {
		if (conjuntoDesconectanteVazio(G, T.qtdV, W)) {
			printf("\nO grafo G nao e conectado.\n");
			return;
		} else {
			aMin = encontrarArestaPesoMin(G, T, W);
			inserirAresta(aMin, &T, W);
			qtdE++;
		}
	}

	return T;
}

int conjuntoDesconectanteVazio(Grafo G, int tam, Vertice W[tam]) {

	int i, j;

	/* Primeiro devo determinar o conjunto desconectante. � aquele que � formado
	pelos v�rtices de G que n�o est�o em T(na verdade no conjunto de v�rtices W de T).
	Comparo cada um dos v�rtices de W(T) com todos os v�rtice de G, aqueles que
	forem diferentes s�o parte do conjunto desconectante.(W[i].id != j+1)
	Em seguida, verifico se existe uma aresta que � formada por um v�rtice de W e
	um v�rtice de G, se existir ent�o o conjunto n�o � vazio.
	(G.matAdj[W[i].id-1][j] != -1)
	*/
	for(i = 0; i < tam; i++) {			// Percorre o conjunto de v�rtices de T, o W.
		for(j = 0; j < G.qtdV; j++) {	// Percorre a Matriz de Adjac�ncias do Grafo G.
			if(W[i].id != j+1) {
				if(G.matAdj[W[i].id-1][j] != -1) {
					return 0;	// O conjunto desconectante n�o � vazio.
				}
			}
		}
	}

	return 1;					// O conjunto desconectante � vazio.
}

Aresta encontrarArestaPesoMin(Grafo G, Grafo T, Vertice W[T.qtdV]) {

	Aresta aMin;
	int pesoMin = INFINITO;
	int i, j, v1, v2;

	/*
	Buscar nas matrizes de adjac�ncias cada poss�vel aresta
	formada por um v�rtice de T(W) e um v�rtice de G.
	*/
	for(i = 0; i < T.qtdV; i++) {		// Percorre o conjunto de v�rtices de t, o W.
		for(j = 0; j < G.qtdV; j++) {	// Percorre a Matriz de Adjac�ncias do Grafo G.

			/*
			FILTRO 1 - Selecionar apenas aquelas arestas que existem (!=-1) e que
			s�o formadas por v�rtice que n�o pertencem a T(G-T) (verticeDesconectado).
			*/
			if(G.matAdj[W[i].id-1][j] != -1 && !verticePertence(T.qtdV, W, j+1)) {

				/*
				FILTRO 2 - Selecionar aquelas arestas cujo peso � menor do que pesoMin
				e ainda n�o foram inseridas na �rvore (== -1).
				*/
				if(G.matAdj[W[i].id-1][j]  < pesoMin && T.matAdj[W[i].id-1][j] == -1) {

					/*
					O v1 da aresta vem da �rvore T(conjunto W) (devido a ordem do for);
					v2 vem da �rvore G (ap�s os filtros) e;
					pesoMin � aquele que se encontra na matAdj de G em (v1,v2).
					*/
					v1 = W[i].id;
					v2 = j+1;
					pesoMin = G.matAdj[v1-1][v2-1];
				}

			}
		}
	}

	aMin.v1.id = v1;
	aMin.v2.id = v2;
	aMin.peso = pesoMin;

	return aMin;
}

//##############################################################################################################################################//
//								+ ALGORITMO DE KRUSKAL +																																																			//
//##############################################################################################################################################//

Grafo algoritmoKruskal(Grafo G);
void ordenarArestas(Aresta E[], int esq, int dir);
int formaCiclo(Aresta aExaminada, Grafo T, int tamanho, Vertice W[tamanho]);
int existeCaminho(int inicio, Grafo T, int parada, int tamanhoG, Vertice W[T.qtdV]);
void inserirArestasEmLista(int tam, int **mat, int qtdE, Aresta E[qtdE]);
int encontrarPosicaoVertice(int id, int tam, Vertice V[tam]);

int algKruskal() {

	int qtd, peso, outroGrafo;
	double tempoGasto;
	char nomeArquivo[100];
	clock_t tInicial, tFinal;
	Grafo G, T;

	srand((unsigned)time(NULL));

	do {
		printf("Qual o nome do arquivo?\n");
		scanf("%s", nomeArquivo);

		// Ler a quantidade de v�rtices do Grafo no arquivo TXT.
		qtd = lerQtdV(nomeArquivo);
		int matAdj[qtd][qtd];

		// Ler a Matriz de Adjac�ncias do arquivo.
		lerMatAdjTXT(nomeArquivo, qtd, matAdj);

		// Copiar a Matriz de Adjac�ncias para o Grafo.
		G = extrairGrafoDeMatriz(qtd, matAdj);

		// Mostrar tamanho do Grafo.(Quantidade de V�rtices e Arestas)
		printf("\nGrafo G\n");
		verificarLeitura(G);

		// Executar algoritmo cronometrando o tempo.
		printf("\nExecutando algoritmo...\n");
		tInicial = clock();
		T = algoritmoKruskal(G);
		tFinal = clock();

		desalocarMatrizAdj(G.qtdV, G.matAdj);

		printf("\nArvore T\n");
		verificarLeitura(T);

		// Calcular peso da �rvore Geradora M�nima T(Tree)
		peso = calcularPeso(T);
		printf("Peso = %d\n", peso);

		desalocarMatrizAdj(T.qtdV, T.matAdj);

		// Mostrar tempo gasto pelo algoritmo.
		tempoGasto = ( (double) (tFinal - tInicial) ) / CLOCKS_PER_SEC;
		printf("\nDemorou %lf segundos.\n", tempoGasto);

		printf("\nDeseja solucionar outro grafo? 0 - Nao/1 - Sim\n");
		scanf("%d", &outroGrafo);
		printf("\n");
	} while(outroGrafo);

	return 0;
}

Grafo algoritmoKruskal(Grafo G) {

	int qtdExam, qtdE, qtdInser;
	Aresta aExaminada;
	qtdE = contarArestas(G.qtdV, G.matAdj);

	Aresta L[qtdE];
	Vertice W[G.qtdV];

	/*
	PASSO 1
	Ordene as arestas em sequ�ncia n�o decrescente por peso em uma lista L, e
	fa�a T = {}.
	*/
	inserirArestasEmLista(G.qtdV, G.matAdj, qtdE, L);
	ordenarArestas(L,0,qtdE-1);
	Grafo T;
	T.matAdj = alocarMatrizAdj(G.qtdV);
	T.qtdV = 0;

	/*
	PASSO 2
	Selecione a primeira aresta de L e inclua em T.
	*/
	inserirAresta(L[0], &T, W);
	qtdExam = 1;
	qtdInser = 1;

	/*
	PASSO 3
	Se toda aresta em L � examinada para poss�vel inclus�o em T, PARE, G n�o �
	conectado. Caso contr�rio, tome a primeira aresta n�o examinada em L. Se esta
	aresta n�o forma um ciclo em T, selecione ela para inclus�o em T e v� para o
	PASSO 4. Caso contr�rio, descarte esta aresta como uma aresta examinada (mas
	n�o selecionada) e repita o PASSO 3.
	PASSO 4
	PARE, se T possui (N-1) arestas. Caso contr�rio, v� para o PASSO 3.
	*/
	while(qtdInser != G.qtdV-1) {
		if(qtdExam == qtdE) {
			printf("G nao e conectado\n");
			return;
		} else {
			aExaminada = L[qtdExam];
			if(!formaCiclo(aExaminada, T, G.qtdV, W)) {
				inserirAresta(aExaminada, &T, W);
				qtdInser++;
			}
			qtdExam++;
		}
	}

	return T;
}

void ordenarArestas(Aresta E[], int esq, int dir) {

	int pivo = esq, i, j, aux;
	Vertice v1, v2;

	// Implementa��o do QUICKSORT.
	for(i = esq+1; i <=dir; i++) {
		j = i;
		if(E[j].peso < E[pivo].peso) {
			aux = E[j].peso;
			v1 = E[j].v1;
			v2 = E[j].v2;
			while(j > pivo) {
				E[j] = E[j-1];
				j--;
			}
			E[j].peso = aux;
			E[j].v1 = v1;
			E[j].v2 = v2;
			pivo++;
		}
	}
	if(pivo-1 >= esq) {
		ordenarArestas(E,esq,pivo-1);
	}
	if(pivo+1 <= dir) {
		ordenarArestas(E,pivo+1,dir);
	}
}

int formaCiclo(Aresta aExaminada, Grafo T, int tamanhoG, Vertice W[T.qtdV]) {

	int v1, v2, i;

	v1 = aExaminada.v1.id;
	v2 = aExaminada.v2.id;
/*
	A avalia��o de uma aresta na �rvore T depende de quatro condi��es:
	1 - v1 n�o est� na �rvore e v2 n�o est� na �rvore - N�O formar� ciclo. INSIRA.
	2 - v1 n�o est� na �rvore e v2     est� na �rvore - N�O formar� ciclo. INSIRA.
	3 - v1     est� na �rvore e v2 n�o est� na �rvore - N�O formar� ciclo. INSIRA.
	4 - v1     est� na �rvore e v2     est� na �rvore - PODE formar ciclo. AVALIE.

	Ou seja apenas no caso em que v1 e v2 perten�am a �rvore T existe a
	POSSIBILIDADE da inclus�o desta aresta formar ciclo. Logo deve-se verificar se
	entre v1 e v2 EXISTE UM CAMINHO na �rvore T, que � o mesmo que avaliar se
	est�o na mesma componente.
*/
	if(verticePertence(T.qtdV, W, v1) && verticePertence(T.qtdV, W, v2)) {

		// Zerar os v�rtices visitados
		for(i = 0; i < T.qtdV; i++) {
			W[i].visitado = 0;
		}

		/*
		A fun��o existeCaminho � feita baseada na t�cnica de BUSCA PROFUNDA por isso
		a necessidade de zerar os v�rtices visitados antes de cada busca.
		*/
		if(existeCaminho(v1-1, T, v2-1, tamanhoG, W)) {
			return 1;
		}
	}

	return 0;
}

int existeCaminho(int inicio, Grafo T, int parada, int tamanhoG, Vertice W[T.qtdV]) {

	int j, pos;

/*
	Com os �ndices da Matriz de Adjac�ncias (por isso � v1-1,v2-1) s�o percorridos
	os v�rtices da �rvore T utilizando uma t�cnica de BUSCA PROFUNDA com in�cio em
	v1 e parada em v2. Se durante a busca, o in�cio for igual a parada significa
	que	EXISTE UM CAMINHO entre v1 e v2, logo a aresta examinada dever� ser
	descartada pois formaria CICLO em caso de inclus�o.
*/
	pos = encontrarPosicaoVertice(inicio+1, T.qtdV, W);
	W[pos].visitado = 1;

	if(inicio == parada) {
		return 1;
	} else {
		for(j = 0; j < tamanhoG; j++) {
			pos = encontrarPosicaoVertice(j+1, T.qtdV, W);

			/*
			Percorre-se a Matriz de Adjac�ncias do Grafo G na linha do v�rtice inicio
			procurando um v�rtice na coluna j onde haja uma aresta entre eles (!=-1) e
			este v�rtice ainda n�o tenha sido visitado (visitado == 0).
			*/
			if(T.matAdj[inicio][j] != -1 && W[pos].visitado == 0) {

				/*
				De posse desse v�rtice j � feita uma chamada recursiva de existeCaminho
				passando-o como in�cio at� encontrar a parada (inicio == parada), ou n�o.
				Se existir caminho a fun��o retornar� 1 recursivamente at� a primeira
				chamada. Caso n�o encontre e tenha percorrido toda a matriz retornar� 0.
				*/
				if(existeCaminho(j, T, parada, tamanhoG, W)) {
					return 1;
				}
			}
		}
	}
	return 0;
}

void inserirArestasEmLista(int tam, int **mat, int qtdE, Aresta E[qtdE]) {

	int i, j;
	qtdE = 0;

	// A partir da Matriz de Adjac�ncias do Grafo G monta uma lista de Arestas.

	for(i = 0; i < tam; i++) {
		for(j = i; j < tam; j++) {
			if(mat[i][j] != -1) {
				E[qtdE].v1.id = i+1;
				E[qtdE].v2.id = j+1;
				E[qtdE].peso = mat[i][j];
				qtdE++;
			}
		}
	}
}


int encontrarPosicaoVertice(int id, int tam, Vertice V[tam]) {

	int i;

	/*
	De posse do id do v�rtice procuro-o na �rvore T(conjunto W de v�rtices) e
	retorno a posi��o(�ndice) correspondente.
	*/

	for(i = 0; i < tam; i++) {
		if(V[i].id == id) {
			return i;
		}
	}
}
