//##############################################################################################################################################//
//								$ GERADORES DE GRAFOS $																																																				//
//##############################################################################################################################################//
/*
#################################################################################################################################################
#																																																																								#
#	>>> Trabalho Final de Grafos;																																																									#
#																																																																								#
#	>>> Participantes:																																																														#
#			> EDSON MACHADO MOITA NETO;																																																								#
#			> PABLO VINICIUS DA CRUZ LIMA;																																																						#
#			> KASSIO VENICIOS ALVES CARVALHO;																																																					#
#			> MARCOS JOSHOA OLIVEIRA PARENTES.																																																				#
#																																																																								#
#################################################################################################################################################
*/

//##############################################################################################################################################//
//						+ GERADOR DE GRAFO NAO ORIENTADO +																																																//
//##############################################################################################################################################//

Grafo gerarGrafoCompleto(int qtdV);
Grafo gerarGrafoQualquer(int qtdV, int densidade);
void garantirConexao(Grafo *G);
void inserirArestasAleatoriamente(Grafo *G, int qtdE);

int geradorGrNOrintado() {

	int qtdG, qtdV, tipoG, i, densidade = 0;
	char nomeArquivo[100], tipoGrafo;
	Grafo G;

	srand((unsigned)time(NULL));

	printf("Deseja gerar quantos grafos?");
	scanf("%d", &qtdG);
	printf("Com quantos vertices?");
	scanf("%d", &qtdV);
	printf("Completo(1) ou qualquer(0)?");
	scanf("%d", &tipoG);

	if(tipoG == 0) {
		printf("Escolha o nivel de densidade:\n");
		printf("1 - Nivel 1 (De |V|-1 ate 25%% de um grafo completo)\n");
		printf("2 - Nivel 2 (De nivel 1 ate 50%% de um grafo completo)\n");
		printf("3 - Nivel 3 (De nivel 2 ate 75%% de um grafo completo)\n");
		printf("4 - Nivel 4 (De nivel 3 ate 1 aresta a menos de um grafo completo)\n");
		scanf("%d", &densidade);
	}

	for(i = 0; i < qtdG; i++) {
		if(tipoG == 1) {
			tipoGrafo = 'C';
			G = gerarGrafoCompleto(qtdV);
		} else if(tipoG == 0) {
			tipoGrafo = 'Q';
			G = gerarGrafoQualquer(qtdV, densidade);
		}

		printf("\nGrafo salvo\n");
		verificarLeitura(G);

		// Montar nome do arquivo de acordo com as escolhas do usu�rio.
		montarNomeArquivo(i+1, qtdV, tipoGrafo, densidade, nomeArquivo);

		// Gravar grafo em arquivo TXT de acordo com nomeArquivo.
		gravarGrafo(G, nomeArquivo);

		desalocarMatrizAdj(G.qtdV, G.matAdj);

		printf("\nCriado o arquivo: ");
		puts(nomeArquivo);
	}

	return 0;
}

Grafo gerarGrafoCompleto(int qtdV) {

	int i, j, peso, pesoMin, pesoMax;
	Grafo G;

	printf("\nGrafo gerado\n|V| = %d\n|E| = %d\n", qtdV, qtdV*(qtdV-1)/2);

	G.qtdV = qtdV;

	pesoMin = 0;
	pesoMax = 10*qtdV;

	G.matAdj = alocarMatrizAdj(qtdV);

	// Inserir pesos aleat�rios em toda a matriz, exceto na diagonal principal.

	for(i = 0; i < G.qtdV; i++) {
		for(j = i; j < G.qtdV; j++) {
			if(i != j) {
				peso = gerarInteiroAleatorio(pesoMin, pesoMax);
				G.matAdj[i][j] = peso;
				G.matAdj[j][i] = peso;
			} else {
				G.matAdj[i][j] = -1;
				G.matAdj[j][i] = -1;
			}
		}
	}

	return G;
}

Grafo gerarGrafoQualquer(int qtdV, int densidade) {

	int arestasMin, arestasMax, qtdE;
	Grafo G;

	arestasMin = qtdV-1; 						// �rvore Gerado M�nima
	arestasMax = (qtdV*(qtdV-1))/2;	// Grafo Completo

	// De acordo com a densidade estipular os limites para quantidade de arestas.

	switch(densidade) {
		case 1:
			arestasMin = qtdV-1;
			arestasMax = arestasMax*0.25;
			break;
		case 2:
			arestasMin = (arestasMax*0.25)+1;
			arestasMax = arestasMax*0.50;
			break;
		case 3:
			arestasMin = (arestasMax*0.50)+1;
			arestasMax = arestasMax*0.75;
			break;
		case 4:
			arestasMin = (arestasMax*0.75)+1;
			arestasMax = arestasMax-1;
			break;
	}

	G.qtdV = qtdV;

	// Determinar aleatoriamente a quantidade de arestas de acordo com os limites.
	qtdE = gerarInteiroAleatorio(arestasMin, arestasMax);

	G.matAdj = alocarMatrizAdj(G.qtdV);

	// Criar um percurso de qtdV-1 v�rtices para garantir que o grafo � conectado.
	garantirConexao(&G);

	// Inserir aleatoriamente as arestas remanescentes (qtdE-(qtdV-1))
	inserirArestasAleatoriamente(&G, qtdE);

	return G;
}

void garantirConexao(Grafo *G) {

	int i, v1, v2, peso, pesoMin, pesoMax, qtdEsc = 0, escolhidos[G->qtdV];

	pesoMin = 0;
	pesoMax = 10*G->qtdV;

	// Escolher aleatoriamente um v�rtice(�ndice) inicial para o percurso.
	v1 = gerarInteiroAleatorio(0, G->qtdV-1);
	escolhidos[qtdEsc] = v1;
	qtdEsc++;

	for(i = 0; i < G->qtdV-1; i++) {

		// Escolher aleatoriamente um v�rtice(�ndice) destino que ainda n�o foi escolhido.
		do {
			v2 = gerarInteiroAleatorio(0, G->qtdV-1);
		} while(foiEscolhido(v2,qtdEsc,escolhidos));

		escolhidos[qtdEsc] = v2;
		qtdEsc++;

		// Inserir peso aleat�rio na aresta formada pelo v�rtice origem e destino.
		peso = gerarInteiroAleatorio(pesoMin, pesoMax);
		G->matAdj[v1][v2] = peso;
		G->matAdj[v2][v1] = peso;

		// V�rtice destino torna-se v�rtice origem para manter grafo conectado.
		v1 = v2;
	}
}

void inserirArestasAleatoriamente(Grafo *G, int qtdE) {

	int i, v1, v2, arestas, peso, pesoMin, pesoMax;

	pesoMin = 0;

	/*
	 Escolhido um limite menor para peso m�ximo para diminuir a possibilidade de
	que as arestas inseridas em garantirConexao sejam aquelas da �rvore Geradora
	m�nima. De toda forma, os pesos naquela e nesta fun��o s�o aleat�rios.
	*/
	pesoMax = 2*G->qtdV;

	// Arestas remanescentes ap�s garantirConexao.
	arestas = qtdE - (G->qtdV-1);

	for(i = 0; i < arestas; i++) {

		/* Escolher v�rtices(�ndices) aleat�rios evitando que: hajam la�os (v1 == v2)
		e, v�rtices(�ndices) que j� tiveram pesos escolhidos (G->matAdj[v1][v2] != -1).
		*/
		do {
			v1 = gerarInteiroAleatorio(0, G->qtdV-1);
			v2 = gerarInteiroAleatorio(0, G->qtdV-1);

		} while(v1 == v2 || G->matAdj[v1][v2] != -1);
		peso = gerarInteiroAleatorio(pesoMin, pesoMax);
		G->matAdj[v1][v2] = peso;
		G->matAdj[v2][v1] = peso;
	}
}
