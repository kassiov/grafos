***
# README #

O objetivo dos programas é testar os algoritmos de Prim, Kruskal e Dijkstra.
Para entender o funcionamento do programa basta ler os relatórios.

***

Funciona somente no Windows (por enquanto).
Basta ter instalado um compilador C para compilar e executar.
Pode ser executado também em IDE's C/C++ como por exemplo:

* Code::Blocks
* Falcon